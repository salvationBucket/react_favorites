const webpack = require('webpack');
const path = require('path');
module.exports = {
  entry:path.join(__dirname,'index.js'),
  output:{
      filename:'bundle.js'
  },
  module:{
    rules:[
      {
         test: /\.(eot|woff|woff2|svg|ttf)([\?]?.*)$/, loader: "file-loader"
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.(scss|css)$/,
          use: [{
            loader: "style-loader"
          }, {
            loader: "css-loader"
          },
          {
            loader: "sass-loader"
          }]
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  devtool:'source-map',
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery"
    })
  ]
}
