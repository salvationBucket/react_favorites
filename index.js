import React from 'react';
import ReactDOM from 'react-dom';
import { createStore,applyMiddleware} from 'redux'
import { Provider } from 'react-redux'
import reducer from './src/reducers'
import thunk from 'redux-thunk';
import {BrowserRouter, Route,Switch,HashRouter} from 'react-router-dom';
import Favorites from './src/routes/favorites';
import ActionsLog from './src/routes/actions_log';
import App from './src/app';
import ReduxToastr from 'react-redux-toastr';
const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducer)}>
    <div>
      <ReduxToastr
      timeOut={4000}
      newestOnTop={false}
      preventDuplicates
      position="top-left"
      transitionIn="fadeIn"
      transitionOut="fadeOut"
      progressBar/>
      <HashRouter>

        <App/>
      </HashRouter>
    </div>  
  </Provider>
  , document.querySelector('.container'));
