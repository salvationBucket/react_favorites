export const TOGGLE_FILTER = 'toggle_filter';
export const SEARCH_FILTER = 'search_filter';
export const ADD_FAVORITE = 'add_favorite';
export const REMOVE_FAVORITE = 'remove_favorite';
export const EDIT_FAVORITE = 'edit_favorite';
export const SET_FAVORITE_TO_REMOVE = 'set_favorite_to_remove';
export const SET_FAVORITE_TO_EDIT = 'set_favorite_to_edit';
export const SEARCH_FAVORITE = 'search_favorite';

export function toggle_filter(payload){
  return {
    type:TOGGLE_FILTER,
    payload
  }
}
export function search_filter(payload){
  return {
    type:SEARCH_FILTER,
    payload
  }
}
export function add_favorite(payload){
  return {
    type:ADD_FAVORITE,
    payload:{
      item:payload,
      logItem:{...payload,type:'added',timestemp:new Date()}
    }
  }
}
export function remove_favorite(payload){
  return {
    type:REMOVE_FAVORITE,
    payload:{
      item:payload.uuid,
      logItem:{...payload,type:'removed',timestemp:new Date()}
    }
  }
}

export function edit_favorite(payload){
  return {
    type:EDIT_FAVORITE,
    payload:{
      item:payload.newItem,
      logItem:{...payload.oldItem,type:'edited',timestemp:new Date()}
    }
  }
}
export function set_favorite_to_remove(payload){
  return {
    type:SET_FAVORITE_TO_REMOVE,
    payload
  }
}
export function set_favorite_to_edit(payload){
  return {
    type:SET_FAVORITE_TO_EDIT,
    payload
  }
}
export function search_favorite(payload){
  return {
    type:SEARCH_FAVORITE,
    payload
  }
}
