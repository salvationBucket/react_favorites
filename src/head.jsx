import React,{Component} from 'react';
import { Link } from 'react-router-dom'
export default class Head extends Component{
  render(){
    return (<nav className="navbar navbar-toggleable-md navbar-light bg-faded">
        <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
      <div className="collapse navbar-collapse" id="navbarText">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link to="/" className="nav-link" >Favorites</Link>
          </li>
          <li className="nav-item">
            <Link to="/logs" className="nav-link">Logs</Link>
          </li>
        </ul>
      </div>
    </nav>
  )}
}
