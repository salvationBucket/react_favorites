import React,{Component} from 'react';
import {connect} from 'react-redux';
import {remove_favorite} from '../actions';
import {toastr} from 'react-redux-toastr';

class DeleteModal extends Component{
  constructor(){
    super();
    this.removeFavorite = this.removeFavorite.bind(this);
  }
  render(){
    return(<div>
      <div className="modal fade" id="deleteModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">Remove favorite website</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
                This action is going to remove this website
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
              <button type="button" className="btn btn-outline-danger" data-dismiss="modal" onClick={this.removeFavorite}>Remove website</button>
            </div>
          </div>
        </div>
      </div>
    </div>)
  }
  removeFavorite(){
    this.props.remove_favorite(this.props.favoriteToRemove);
    toastr.success('Opertaion completed', 'Website removed');
  }
}
function mapStateToProps(state){
  return {
    favoriteToRemove:state.Favorites.favoriteToRemove
  }
}
export default connect(mapStateToProps,{remove_favorite})(DeleteModal)
