import React,{Component} from 'react';
import {connect} from 'react-redux';
import {search_favorite} from '../actions';

class SearchBar extends Component{
  constructor(props){
    super();
    this.state = {
      searchTerm:props.searchTerm
    }
    this.onSearchChange = this.onSearchChange.bind(this);
  }
  onSearchChange(event){
    this.setState({searchTerm:event.target.value});
    this.props.search_favorite(event.target.value);
  }
  render(){
      return(<div className="input-group">
      <input type="text" value={this.state.searchTerm} onChange={this.onSearchChange} className="form-control" placeholder="Search for..."/>
      <span className="input-group-btn">
        <button className="btn btn-secondary" type="button"><i className="icon-magnifier"/></button>
      </span>
    </div>)
  }
  componentWillReceiveProps({searchTerm}) {
    if (searchTerm) {
      this.setState({
        searchTerm
      })
    }
  }
}

function mapStateToProps(state){
  return {
    searchTerm:state.Favorites.searchTerm
  }
}
export default connect(mapStateToProps,{search_favorite})(SearchBar);
