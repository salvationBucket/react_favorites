import React,{Component} from 'react';
import {connect} from 'react-redux';
import {set_favorite_to_remove,set_favorite_to_edit} from '../actions';

class Tile extends Component{
  constructor(){
    super();
    this.setFavToRemove = this.setFavToRemove.bind(this);
    this.setFavToEdit = this.setFavToEdit.bind(this);
  }
  render(){
      return(<div className="card">
      <div className="card-block">
        <h4 className="card-title text-info">{this.props.info.name}</h4>
        <p className="card-text">{this.props.info.url}</p>
        <a className="ml-3 btn pointer">
          <i className="icon-note" data-toggle="modal" onClick={this.setFavToEdit} data-target="#editModal"/>
        </a>
        <a className="btn pointer">
          <i className="icon-trash" data-toggle="modal" onClick={this.setFavToRemove} data-target="#deleteModal"/>
        </a>
      </div>
    </div>
  )}
  setFavToEdit(){
    this.props.set_favorite_to_edit(this.props.info);
  }
  setFavToRemove(){
    this.props.set_favorite_to_remove(this.props.info);
  }
}
export default connect(null,{set_favorite_to_remove,set_favorite_to_edit})(Tile)
