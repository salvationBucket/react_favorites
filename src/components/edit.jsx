import React,{Component} from 'react';
import {connect} from 'react-redux';
import {edit_favorite} from '../actions';
import {toastr} from 'react-redux-toastr';

class EditModal extends Component{
  constructor(){
    super();
    this.setName = this.setName.bind(this);
    this.setUrl = this.setUrl.bind(this);
    this.editFavoite = this.editFavoite.bind(this);
    this.state = {
      newItem:{
        name:'',
        url:'',
        uuid:''
      },
      oldItem:{}
    };
  }
  render(){
    return(<div>
      <div className="modal fade" id="editModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">Edit website</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form>
                <div className="form-group">
                  <label htmlFor="name">Website name</label>
                  <input type="text" value={this.state.newItem.name} onChange={this.setName} className="form-control" id="name" placeholder="Enter name"/>
                </div>
                <div className="form-group">
                  <label htmlFor="url">Url</label>
                  <input type="text" value={this.state.newItem.url} onChange={this.setUrl} className="form-control" id="url" placeholder="Enter url"/>
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
              <button type="button" className="btn btn-outline-info" data-dismiss="modal" onClick={this.editFavoite}>Save changes</button>
            </div>
          </div>
        </div>
      </div>
    </div>)
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.info) {
      this.setState({
        newItem:nextProps.info,
        oldItem:nextProps.info
      })
    }
  }
  editFavoite(){
    this.props.edit_favorite(this.state);
    toastr.success('Opertaion completed', 'Website was updated');
  }
  setName(event){
    this.setState(
      {...this.state,newItem:
        {...this.state.newItem,name:event.target.value}}
      );
  }
  setUrl(event){
    this.setState(
      {...this.state,newItem:
        {...this.state.newItem,url:event.target.value}}
      );
  }
}
function mapStateToProps(state){
  return {
    info:state.Favorites.favoriteToEdit
  }
}
export default connect(mapStateToProps,{edit_favorite})(EditModal)
