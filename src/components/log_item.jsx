import React,{Component} from 'react';
import {connect} from 'react-redux';
import {set_favorite_to_remove,set_favorite_to_edit} from '../actions';

export default class LogItem extends Component{
  render(){
      let name = this.props.info.name;
      let url = this.props.info.url;
      let date = this.props.info.timestemp.toLocaleDateString();
      let time = this.props.info.timestemp.toLocaleTimeString();
      let action = this.props.info.type;

      return(<div className='list-group-item list-group-item-action'>
            <div className="col-md-3">{date} {time}</div>
            <div className="col-md-3">{action}</div>
            <div className="col-md-3">{name}</div>
            <div className="col-md-3">{url}</div>
      </div>
  )}
}
