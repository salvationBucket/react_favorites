import React,{Component} from 'react';
import {connect} from 'react-redux';
import {set_favorite_to_remove,set_favorite_to_edit} from '../actions';

class ListItem extends Component{
  constructor(){
    super();
    this.setFavToRemove = this.setFavToRemove.bind(this);
    this.setFavToEdit = this.setFavToEdit.bind(this);
  }

  render(){
      let name = this.props.info.name;
      let url = this.props.info.url;

      return(<div className='list-group-item list-group-item-action'>
            <div className="col-md-4 col-sm-4"><i className="icon-star"/> {name}</div>
            <div className="col-md-4 col-sm-4">{url}</div>
            <div className="col-md-4 col-sm-4 d-flex flex-row-reverse">
              <a className="ml-3 pointer">
                <i className="icon-note " data-toggle="modal" onClick={this.setFavToEdit} data-target="#editModal"/>
              </a>
              <a className="pointer">
                <i className="icon-trash" data-toggle="modal" onClick={this.setFavToRemove} data-target="#deleteModal"/>
              </a>
            </div>
      </div>
  )}
  setFavToEdit(){
    this.props.set_favorite_to_edit(this.props.info);
  }
  setFavToRemove(){
    this.props.set_favorite_to_remove(this.props.info);
  }
}
export default connect(null,{set_favorite_to_remove,set_favorite_to_edit})(ListItem)
