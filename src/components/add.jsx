import React,{Component} from 'react';
import {connect} from 'react-redux';
import {add_favorite} from '../actions';
import {toastr} from 'react-redux-toastr';
import  v1 from 'uuid' ;
class AddModal extends Component{
  constructor(){
    super();
    this.state = {
      name:'',
      url:'',
      uuid:v1()
    }
    this.setName = this.setName.bind(this);
    this.setUrl = this.setUrl.bind(this);
    this.addFavorite = this.addFavorite.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(){
    var form = document.getElementById('favform');

    if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
        form.classList.add('was-validated');
        toastr.error('Opertaion faild', 'Please fill out all the fields');
    }
    else{
      this.addFavorite();
      form.classList.remove('was-validated');
      toastr.success('Opertaion completed', 'New website was added');

    }

  }
  render(){
    return(<div>
      <div className="modal fade" id="addModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">Add new website</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form id="favform" onSubmit={()=>{this.onSubmit()}} noValidate>
                <div className="form-group">
                  <label htmlFor="name">Website name</label>
                  <input required type="text" value={this.state.name} onChange={this.setName} className="form-control" id="name" placeholder="Enter name"/>
                </div>
                <div className="form-group">
                  <label htmlFor="url">Url</label>
                  <input required type="text" value={this.state.url} onChange={this.setUrl} className="form-control" id="url" placeholder="Enter url"/>

                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" form="favform" className="btn btn-outline-info">Add website</button>
            </div>
          </div>
        </div>
      </div>
    </div>)
  }
  addFavorite(){
    this.props.add_favorite(this.state);
    this.setState( {
      name:'',
      url:'',
      uuid:v1()
    });
  }
  setName(event){
    this.setState({name:event.target.value});
  }
  setUrl(event){
    this.setState({url:event.target.value});
  }
}

export default connect(null,{add_favorite})(AddModal)
