import {
  TOGGLE_FILTER,
  SEARCH_FILTER,
  ADD_FAVORITE,
  EDIT_FAVORITE,
  REMOVE_FAVORITE,
  SET_FAVORITE_TO_REMOVE,
  SET_FAVORITE_TO_EDIT,
  SEARCH_FAVORITE
} from '../actions';
const initialState = {
  selectedFilter : 'grid',
  favoriteToRemove:'',
  favoriteToEdit:{},
  actions:[],
  favorites:[
    {uuid:'4382c49f-1742-4f83-8309-f6eba50dea51',name:'Google',url:'www.google.com'},
    {uuid:'3344c49f-1742-4f83-8309-f6eba50dea51',name:'Microsoft',url:'www.microsoft.com'},
    {uuid:'33123123-1742-4f83-8309-f6eba50dea51',name:'Walla',url:'www.walla.com'},
    {uuid:'asad2133-1742-4f83-8309-f6eba50dea51',name:'Ynet',url:'www.ynet.co.il'},
    {uuid:'fgg23452-1742-4f83-8309-f6eba50dea51',name:'Gamespot',url:'www.gamespot.com'},
    {uuid:'52313asd-1742-4f83-8309-f6eba50dea51',name:'Nana',url:'www.nana.co.il'}],
  favDisplayed:[],
  searchTerm:'',
  new_img:''
}

export default function(state = initialState,action){
  switch (action.type) {
    case TOGGLE_FILTER:
      return {...state,selectedFilter:action.payload};
      break;
    case SEARCH_FILTER:
      return {...state,selectedFilter:action.payload};
      break;
    case ADD_FAVORITE:
      var newFavs = [...state.favorites,action.payload.item];
      var favDisplayed = filter(newFavs,state.searchTerm);
      return {...state,favorites:newFavs,
      actions:[...state.actions,action.payload.logItem],
      favDisplayed};
      break;
    case EDIT_FAVORITE:
      var newFavs = state.favorites.map((item)=>{
        if(action.payload.item.uuid == item.uuid){
          return action.payload.item;
        }
        return item;
      })
      var favDisplayed = filter(newFavs,state.searchTerm);
      return {...state,favorites:newFavs,
        actions:[...state.actions,action.payload.logItem],
        favDisplayed};
      break;
    case REMOVE_FAVORITE:
      var res = state.favorites.filter(item => item.uuid !== action.payload.item);
      var favDisplayed = filter(res,state.searchTerm);
      return {...state,favorites:res,
        actions:[...state.actions,action.payload.logItem],
        favoriteToRemove:{},
        favDisplayed};
      break;
    case SET_FAVORITE_TO_REMOVE:
      return {...state,favoriteToRemove:action.payload};
      break;
    case SET_FAVORITE_TO_EDIT:
      return {...state,favoriteToEdit:action.payload};
      break;
    case SEARCH_FAVORITE:
      var favDisplayed = filter(state.favorites,action.payload);
      return {...state,favDisplayed,searchTerm:action.payload};
      break;
    default:
      var favDisplayed = filter(state.favorites);
      return {...state,favDisplayed};
  }
}
function filter(favorites,searchTerm = ''){
  var favDisplayed = [];
  if(searchTerm != ''){
    favDisplayed = favorites.filter(item => item.url.includes(searchTerm) || item.name.includes(searchTerm));
  }
  else{
    favDisplayed = [...favorites];
  }
  return favDisplayed;
}
