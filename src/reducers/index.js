import { combineReducers } from 'redux'
import Favorites from './reducer_favorites';
import {reducer as toastrReducer} from 'react-redux-toastr'
const reducers = combineReducers({
  Favorites,
  toastr: toastrReducer
})

export default reducers
