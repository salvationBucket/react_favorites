import React from 'react'
import Head from './head'
import Main from './main'
import 'simple-line-icons/scss/simple-line-icons.scss';
import 'animate.css/animate.min.css';
import '../style/app.scss';
const App = () => (
  <div>
    <Head />
    <Main />
  </div>
)

export default App
