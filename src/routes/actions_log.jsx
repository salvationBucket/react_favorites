import React,{Component} from 'react';
import Logs from '../containers/logs';
import {connect} from 'react-redux';

class ActionsLog extends Component{
  render(){
      return(<div className="mt-4">
        <h1 className="mb-5">Actions log</h1>
        <Logs actions={this.props.actions}/>
      </div>)
  }
}
function mapStateToProps(state){
  return {
    actions:state.Favorites.actions
  }
}
export default connect(mapStateToProps,null)(ActionsLog);
