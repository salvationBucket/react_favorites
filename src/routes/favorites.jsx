import React,{Component} from 'react'
import Tile from '../components/tile';
import {connect} from 'react-redux';
import {toggle_filter} from '../actions';
import ListView from '../containers/list_view';
import TileView from '../containers/tile_view';
import SearchBar from '../components/search_bar';
import AddModal from '../components/add';
import DeleteModal from '../components/delete';
import EditModal from '../components/edit';

const FILTER_GRID = 'grid';
const FILTER_LIST = 'list';

class Favorites extends Component{
  isActive(filter){
      return filter == this.props.selectedFilter ? 'active' :'';
  }
  render(){
      return(<div className="mt-4">
          <h1>Favorite Sites</h1>
        <div className="row mt-5">
          <div className="col-md-6">
            <div className="btn-group">
              <a className={`btn btn-outline-secondary ${this.isActive(FILTER_GRID)}`} onClick={()=>{this.props.toggle_filter(FILTER_GRID)}}>
                <i className="icon-grid"/>
              </a>
              <a className={`btn btn-outline-secondary ${this.isActive(FILTER_LIST)}`} onClick={()=>{this.props.toggle_filter(FILTER_LIST)}}>
                <i className="icon-list"/>
              </a>
              <a className="btn btn-outline-secondary" data-toggle="modal" data-target="#addModal"><i className="icon-plus"/> Add</a>
            </div>
          </div>
          <div className="col-md-6">
            <SearchBar/>
          </div>
        </div>
        <div className="row mt-5">
          <div className="col-md-12">
            {
              this.renderFilterView()}
          </div>
        </div>
        <AddModal/>
        <DeleteModal />
        <EditModal/>
      </div>
    )
  }
  renderFilterView(){
    return(<div>
          {(() => {
              switch (this.props.selectedFilter) {
                  case FILTER_LIST:
                      return <ListView items={this.props.favDisplayed}/>
                  case FILTER_GRID:
                      return <TileView items={this.props.favDisplayed}/>
              }
          })()}
      </div>);
  }
}
function mapStateToProps(state){
  return{
    selectedFilter:state.Favorites.selectedFilter,
    favDisplayed:state.Favorites.favDisplayed
  }
}
export default connect(mapStateToProps,{toggle_filter})(Favorites)
