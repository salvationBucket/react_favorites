import React,{Component} from 'react'
import Tile from '../components/tile'
export default class TileView extends Component{
  render(){
      return(<div>
        <div className="row">
          {this.props.items.map((info,index)=>{
            return <div className="col-md-4 col-sm-6 col-lg-3 pt-2 animated" key={index}><Tile info={info} /></div>
          })}
        </div>
      </div>
  )}
}
