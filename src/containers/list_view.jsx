import React,{Component} from 'react'
import ListItem from '../components/list_item';
export default class ListView extends Component{
  constructor(){
    super();
  }

  render(){
      return(<div>
        <div className="list-group">
          <div className='list-group-item list-group-item-action'>
                <div className="col-md-4 col-sm-6 col-lg-4 text-info">Website name</div>
                <div className="col-md-4 col-sm-6 col-lg-4 text-info">URL</div>
                <div className="col-md-4 col-lg-4"></div>
          </div>
          {this.props.items.map((info,index)=>{
            return <ListItem info={info} key={index}/>
          })}
        </div>
      </div>
  )}
}
