import React,{Component} from 'react'
import LogItem from '../components/log_item';
export default class Logs extends Component{
  constructor(){
    super();
  }

  render(){
      return(<div>
        <div className="list-group">
          <div className='list-group-item list-group-item-action text-info'>
                <div className="col-md-3">Date/Time</div>
                <div className="col-md-3">Action</div>
                <div className="col-md-3">Website name</div>
                <div className="col-md-3">URL</div>
          </div>
          {this.props.actions.map((info,index)=>{
            return <LogItem info={info} key={index}/>
          })}
        </div>
      </div>
  )}
}
