import React from 'react'
import { Switch, Route } from 'react-router-dom'
import ActionLog from './routes/actions_log'
import Favorites from './routes/favorites'

const Main = () => (
  <main>
    <Switch>
      <Route exact path='/' component={Favorites}/>
      <Route exact path='/logs' component={ActionLog}/>
    </Switch>
  </main>
)

export default Main
